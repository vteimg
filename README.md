![Screenshot](https://pbs.twimg.com/media/A7q1J_ACMAELNR2.png)

![Screenshot](https://pbs.twimg.com/media/A7rOCoPCYAArW5o.png)

This is a patch to libvte's 0.2x.x series that allows you to have inline images
in your terminal. An acompanying program, _view_, uses GdkPixbuf to load several
types of images and sends them to this terminal extension.
