CFLAGS:=-Wall -O2

view_cflags:=$(shell pkg-config --cflags glib-2.0 gio-unix-2.0 gdk-pixbuf-2.0)
view_libs:=$(shell pkg-config --libs glib-2.0 gio-unix-2.0 gdk-pixbuf-2.0) -lm

objs:=view.o

all: view

view: $(objs)
	$(CC) $(LDFLAGS) -o $@ $+ $(view_libs)

$(objs): %.o: %.c
	$(CC) $(view_cflags) $(CFLAGS) -o $@ -c $<

clean:
	rm -f view *.o

.PHONY: all clean
